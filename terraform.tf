terraform {
  backend "s3" {
    bucket = "tfseif"
    key    = "terraform.tfstate"
    region = "eu-west-2"
  }

}

