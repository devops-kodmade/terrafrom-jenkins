resource "aws_vpc" "vpc" {
  cidr_block       = "10.22.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "vpc-terra"
  }
}

resource "aws_subnet" "subnet" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = "10.22.1.0/24"
  # pour autoriser l'affectation des adresse public automatiquement aux EC2 dans le subnet
  map_public_ip_on_launch = true
  tags = {
    Name = "subnet-terra"
  }
}


resource "aws_internet_gateway" "internet" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "internet-terra"
  }
}

resource "aws_route_table" "route" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet.id
  }
  #le routage vers le local est automatique

  tags = {
    Name = "route-table-terra"
  }
}


resource "aws_route_table_association" "association" {
  subnet_id      = aws_subnet.subnet.id
  route_table_id = aws_route_table.route.id
}


resource "aws_security_group" "groupeS" {
  name        = "groupeS"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "groupeS-terra"
  }
}


resource "aws_instance" "instance3" {
  ami           = "ami-0f540e9f488cfa27d"
  instance_type = "t2.micro"
  key_name      = aws_key_pair.pub-key.key_name
  tags = {
    Name = var.nom-instance
  }
  subnet_id              = aws_subnet.subnet.id
  #Spercifier le sécurité groupe poru l'instance (très important pour pourvoir connecter en ssh sinon il prend le sécurité groupe par défault)
  vpc_security_group_ids = [aws_security_group.groupeS.id]
}

#creer les key pair
resource "tls_private_key" "key_pair" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

#declarer les key pair dans aws
resource "aws_key_pair" "pub-key" {
  key_name   = "key-pair"
  public_key = tls_private_key.key_pair.public_key_openssh

}


#resource "aws_s3_bucket" "backend" {
#  bucket = "tfseif"
#lifecycle { prevent_destroy = true }
#}
 



